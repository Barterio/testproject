package ua.appsoft.testproject;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;

import ua.appsoft.testproject.ui.apps.top.TopFragment;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        swapFragment(new TopFragment());
    }

    public void swapFragment(Fragment f) {
        this.getSupportFragmentManager()
                .beginTransaction()
                .addToBackStack(null)
                .replace(R.id.fragment_container, f)
                .commit();
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
