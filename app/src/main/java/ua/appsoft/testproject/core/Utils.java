package ua.appsoft.testproject.core;

import ua.appsoft.testproject.App;

/**
 * <b>Copyright (C) Appsoft, Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Bogdan Artemenko <artemenkobogdan@gmail.com> on  on 6/15/16.</b>
 */
public class Utils {
    public static boolean isResponceValid(String responce) {
        try {
            App.gson.fromJson(responce, Object.class);
            return true;
        } catch (com.google.gson.JsonSyntaxException ex) {
            return false;
        }
    }
}
