package ua.appsoft.testproject.core.http;

import com.android.volley.Request;

import ua.appsoft.testproject.App;

/**
 * <b>Copyright (C) Appsoft, Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Bogdan Artemenko <artemenkobogdan@gmail.com> on  on 10/22/15.</b>
 */
public class HttpClient {
    private static final String TAG = "HTTPClient";
    public static HttpClient instance;

    private HttpClient() {
    }

    public static HttpClient getInstance() {
        if (instance == null)
            instance = new HttpClient();
        return instance;
    }

    public void getList(JsonObjectResponseListener.ResponceStatusHandler handler) {
        jsonGet(Api.BASE_URL, handler);
    }

    public void jsonGet(String url, JsonObjectResponseListener.ResponceStatusHandler handler) {
        App.requestQueue.add(
                new CustomJsonObjectRequest(
                        Request.Method.GET,
                        url,
                        "",
                        new JsonObjectResponseListener(handler)));
    }

    public void jsonPost(String url, String body, JsonObjectResponseListener.ResponceStatusHandler handler) {
        App.requestQueue.add(
                new CustomJsonObjectRequest(
                        Request.Method.POST,
                        url,
                        body,
                        new JsonObjectResponseListener(handler)));
    }

    public void jsonDelete(String url, String body, JsonObjectResponseListener.ResponceStatusHandler handler) {
        App.requestQueue.add(
                new CustomJsonObjectRequest(
                        Request.Method.DELETE,
                        url,
                        body,
                        new JsonObjectResponseListener(handler)
                ));
    }

    private interface Api {
        String BASE_URL = "http://demigos.com/test/releases.html";
    }
}
