package ua.appsoft.testproject.core.http;

import android.util.Log;

import com.android.volley.Response;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import ua.appsoft.testproject.App;
import ua.appsoft.testproject.core.Utils;

/**
 * <b>Copyright (C) Appsoft, Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Bogdan Artemenko <artemenkobogdan@gmail.com> on  on 10/28/15.</b>
 */
public class JsonObjectResponseListener implements Response.Listener {

    private static final String TAG = "ResponseListener";
    private ResponceStatusHandler mHandler;

    public JsonObjectResponseListener(ResponceStatusHandler handler) {
        mHandler = handler;
    }

    @Override
    public void onResponse(Object response) {
        if (mHandler == null)
            return;
        String q = response.toString();
        if (Utils.isResponceValid(q)) {
            JsonObject jo = App.gsonParser.parse(q).getAsJsonObject();
            Log.d(TAG, jo.toString());
            switch (jo.get("status").getAsString()) {
                case "1":
                    mHandler.success(jo.getAsJsonArray("data"));
                    break;
                default:
                    JsonObject error = jo.getAsJsonObject("data");
                    mHandler.error(error.get("code").toString(), error.get("message").getAsString());
                    break;
            }
        }
    }

    public boolean onNetworkError(String code, String message) {
        if (mHandler == null) return false;
        return mHandler.error(code, message);
    }

    public interface ResponceStatusHandler {
        void success(JsonArray data);

        boolean error(String errorCode, String errorMessage);
    }
}
