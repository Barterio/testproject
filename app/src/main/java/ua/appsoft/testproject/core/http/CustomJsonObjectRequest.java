package ua.appsoft.testproject.core.http;

import android.util.Log;

import com.android.volley.NoConnectionError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

/**
 * <b>Copyright (C) Appsoft, Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Bogdan Artemenko <artemenkobogdan@gmail.com> on  on 10/29/15.</b>
 */
public class CustomJsonObjectRequest extends JsonObjectRequest {
    private static final String TAG = "CustomJsonObjectRequest";
    private JsonObjectResponseListener mListener;

    public CustomJsonObjectRequest(int method, String url, String body, final JsonObjectResponseListener listener) {
        super(method, url != null ? url : "", body != null ? body : "", listener, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error instanceof NoConnectionError) {
                    if (listener != null) {
                        String errorCode = "4";
                        if (error != null)
                            if (error.networkResponse != null)
                                errorCode = String.valueOf(error.networkResponse.statusCode);
                        listener.onNetworkError(errorCode, "No Internet Connection");
                    }
                    Log.e(TAG, "NO INTERNET");
                }
            }
        });
        mListener = listener;
        Log.d(TAG, url + "\n" + body);
    }
}
