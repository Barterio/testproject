package ua.appsoft.testproject.model;

import com.google.gson.JsonObject;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.ArrayList;

import ua.appsoft.testproject.App;

/**
 * <b>Copyright (C) Appsoft, Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Bogdan Artemenko <artemenkobogdan@gmail.com> on  on 6/16/16.</b>
 */
public class Product implements Serializable {
    private static final String TAG = "Product";
    @Expose
    @SerializedName("title")
    private String mTitle;
    @Expose
    @SerializedName("date")
    private String mDate;
    @Expose
    @SerializedName("author")
    private String mAuthor;
    @Expose
    @SerializedName("description")
    private String mDescription;
    @Expose
    @SerializedName("image")
    private String mImageUrl;

    public Product() {
        super();
    }

    public Product(JsonObject object) {
        super();
        Product p = App.gson.fromJson(object, Product.class);
        mTitle = p.getTitle();
        mDate = p.getDate();
        mAuthor = p.getAuthor();
        mDescription = p.getDescription();
        mImageUrl = p.getImageUrl();
    }

    public static Type getObjectType() {
        return new TypeToken<ArrayList<Product>>() {
        }.getType();
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public String getDate() {
        return mDate;
    }

    public void setDate(String date) {
        mDate = date;
    }

    public String getAuthor() {
        return mAuthor;
    }

    public void setAuthor(String author) {
        mAuthor = author;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    public String getImageUrl() {
        return mImageUrl;
    }

    public void setImageUrl(String imageUrl) {
        mImageUrl = imageUrl;
    }
}
