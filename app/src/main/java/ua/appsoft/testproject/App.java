package ua.appsoft.testproject;

import android.app.Application;
import android.content.Context;
import android.graphics.Bitmap;
import android.util.LruCache;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonParser;

import ua.appsoft.testproject.core.http.HttpClient;

/**
 * <b>Copyright (C) Appsoft, Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Bogdan Artemenko <artemenkobogdan@gmail.com> on  on 6/15/16.</b>
 */
public class App extends Application {
    public static Gson gson;
    public static RequestQueue requestQueue;
    public static ImageLoader imageLoader;
    public static HttpClient httpClient;
    public static Context context;
    public static JsonParser gsonParser;

    @Override
    public void onCreate() {
        super.onCreate();
        init(this);
    }

    private void init(Context context) {
        this.context = context;
        requestQueue = Volley.newRequestQueue(context);
        imageLoader = setUpImageLoader();
        gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().setPrettyPrinting().create();
        httpClient = HttpClient.getInstance();
        gsonParser = new JsonParser();
    }

    private ImageLoader setUpImageLoader() {
        return new ImageLoader(requestQueue,
                new ImageLoader.ImageCache() {
                    private final LruCache<String, Bitmap>
                            cache = new LruCache<String, Bitmap>(20);

                    @Override
                    public Bitmap getBitmap(String url) {
                        return cache.get(url);
                    }

                    @Override
                    public void putBitmap(String url, Bitmap bitmap) {
                        cache.put(url, bitmap);
                    }
                });
    }
}
