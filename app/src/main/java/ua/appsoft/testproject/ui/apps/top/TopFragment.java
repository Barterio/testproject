package ua.appsoft.testproject.ui.apps.top;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.JsonArray;

import java.util.ArrayList;
import java.util.List;

import ua.appsoft.testproject.App;
import ua.appsoft.testproject.R;
import ua.appsoft.testproject.core.http.JsonObjectResponseListener;
import ua.appsoft.testproject.model.Product;

/**
 * A simple {@link Fragment} subclass.
 */
public class TopFragment extends Fragment {
    private RecyclerView mRecyclerView;
    private ProductRecyclerViewAdapter mAdapter;
    private List<Product> mList;

    public TopFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_top, container, false);
        initViews(v);
        initLists();
        return v;
    }

    private void initViews(View v) {
        mRecyclerView = (RecyclerView) v.findViewById(R.id.topRecyclerView);
    }

    private void initLists() {
        mList = new ArrayList<>();
        App.httpClient.getList(new JsonObjectResponseListener.ResponceStatusHandler() {
            @Override
            public void success(JsonArray data) {
                mList = App.gson.fromJson(data, Product.getObjectType());
                initRecyclerView();
            }

            @Override
            public boolean error(String errorCode, String errorMessage) {
                return false;
            }
        });
    }

    private void initRecyclerView() {
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        LinearLayoutManager llm = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(llm);
        mAdapter = new ProductRecyclerViewAdapter(getContext(), mList);
        mRecyclerView.setAdapter(mAdapter);
    }

}
