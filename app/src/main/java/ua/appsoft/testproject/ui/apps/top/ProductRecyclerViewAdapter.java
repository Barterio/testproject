package ua.appsoft.testproject.ui.apps.top;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import ua.appsoft.testproject.R;
import ua.appsoft.testproject.model.Product;

/**
 * <b>Copyright (C) Appsoft, Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Bogdan Artemenko <artemenkobogdan@gmail.com> on  on 6/16/16.</b>
 */
public class ProductRecyclerViewAdapter extends RecyclerView.Adapter<ProductRecyclerViewAdapter.ProductCardViewHolder> {
    private List<Product> mList;
    private Context mContext;

    public ProductRecyclerViewAdapter(Context c, List<Product> products) {
        mContext = c;
        mList = products;
    }

    @Override
    public ProductCardViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ProductCardViewHolder(LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.layout_product_list_item,
                        parent,
                        false));
    }

    @Override
    public void onBindViewHolder(ProductCardViewHolder holder, int position) {
        Product p = mList.get(position);
        Picasso.with(mContext).load(p.getImageUrl()).into(holder.image);
        holder.title.setText(p.getTitle());
        holder.date.setText(p.getDate());
        holder.author.setText(p.getAuthor());
        holder.description.setText(p.getDescription());
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    class ProductCardViewHolder extends RecyclerView.ViewHolder {
        ImageView image;
        TextView title, date, author, description;

        public ProductCardViewHolder(View v) {
            super(v);
            image = (ImageView) v.findViewById(R.id.productListItemImage);
            title = (TextView) v.findViewById(R.id.productListItemTitle);
            date = (TextView) v.findViewById(R.id.productListItemDate);
            author = (TextView) v.findViewById(R.id.productListItemAuthor);
            description = (TextView) v.findViewById(R.id.productListItemDescription);
        }
    }
}
