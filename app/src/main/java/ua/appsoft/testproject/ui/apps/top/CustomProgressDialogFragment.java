package ua.appsoft.testproject.ui.apps.top;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;

/**
 * <b>Copyright (C) Appsoft, Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Bogdan Artemenko <artemenkobogdan@gmail.com> on  on 11/10/15.</b>
 */
public class CustomProgressDialogFragment extends DialogFragment {
    private static String mMessage;

    public static CustomProgressDialogFragment getInstance(String message) {
        CustomProgressDialogFragment fragment = new CustomProgressDialogFragment();
        mMessage = message;
        return fragment;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        ProgressDialog dialog = new ProgressDialog(getActivity());
        dialog.setMessage(mMessage);
        dialog.setIndeterminate(true);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        return dialog;
    }

    public void setText(String text) {
        mMessage = text;
    }

    @Override
    public void dismiss() {
        if (this != null)
            super.dismiss();
    }
}
